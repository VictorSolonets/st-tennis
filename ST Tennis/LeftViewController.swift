//
//  LeftViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import ReachabilitySwift
import FirebaseAuth

enum LeftMenu: Int {
    case main = 0
    case matches
    case chat
    case logout
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

@available(iOS 10.0, *)
class LeftViewController : UIViewController, LeftMenuProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var menus = [["Main":"mainIcon"], ["Matches":"matchesIcon"], ["Chat":"chatIcon"], ["Logout":"logoutIcon"]]

    var profileViewController: UIViewController!
    var mainViewController: UIViewController!
    var invitesViewController: UIViewController!
    var channelListViewController: UIViewController!
    
    var imageHeaderView: ImageHeaderView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor.lightGray
        
        self.tableView.backgroundColor = UIColor(hex: "B71E22", alpha: 0.3)
        self.view.backgroundColor = UIColor(hex: "B71E22", alpha: 0.3)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let invitesViewController = storyboard.instantiateViewController(withIdentifier: "InvitesViewController") as! InvitesViewController
        self.invitesViewController = UINavigationController(rootViewController: invitesViewController)
        
        let chatViewController = storyboard.instantiateViewController(withIdentifier: "ChannelListViewController") as! ChannelListViewController
        self.channelListViewController = UINavigationController(rootViewController: chatViewController)
        
        self.tableView.registerCellClass(BaseTableViewCell.self)
        
        self.imageHeaderView = ImageHeaderView.loadNib()
        
        let recognizer = UITapGestureRecognizer.init(target: self, action: #selector(self.openProfileAction))
        self.imageHeaderView.gestureRecognizers = [recognizer]

        self.imageHeaderView.name.text = User.sharedInstance.userName
        
        self.view.addSubview(self.imageHeaderView)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if User.sharedInstance.avatarURL != nil {
            if let url = NSURL(string: User.sharedInstance.avatarURL!) as URL? {
                self.imageHeaderView.setProfileImage(avatarURL:url as NSURL)
            }
        } else {
            self.imageHeaderView.avatarImage.image = UIImage.init(named: "mainRocket")
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }
    
    func openProfileAction () {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let profileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.profileViewController = UINavigationController(rootViewController: profileViewController)
        self.slideMenuController()?.changeMainViewController(self.profileViewController, close: true)
    }
    
    func changeViewController(_ menu: LeftMenu) {
        switch menu {
        case .main:
            self.slideMenuController()?.changeMainViewController(self.mainViewController, close: true)
        case .matches:
            self.slideMenuController()?.changeMainViewController(self.invitesViewController, close: true)
        case .chat:
            self.slideMenuController()?.changeMainViewController(self.channelListViewController, close: true)
        case .logout:
            let reachability = Reachability()!
            if reachability.isReachable {
                do {
                    try FIRAuth.auth()?.signOut()
                    self.cleanKeys()
                } catch {
                    print("Error")
                }

                self.popToLoginViewController()
            } else {
                let alert = UIAlertController(title: "Error", message: "Can't logout without internet", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func cleanKeys() {
        let login = FBSDKLoginManager()
        login.loginBehavior = FBSDKLoginBehavior.systemAccount
        login.logOut()
        UserDefaults.standard.removeObject(forKey: "facebookID")
        UserDefaults.standard.removeObject(forKey: "ProfileDescription")
        UserDefaults.standard.removeObject(forKey: "FullName")
        UserDefaults.standard.removeObject(forKey: "avatarUrl")
    }
    
    func popToLoginViewController () {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UIApplication.shared.keyWindow?.rootViewController = loginViewController
        UIApplication.shared.setStatusBarHidden(false, with: .none)
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
}


@available(iOS 10.0, *)
extension LeftViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .matches, .chat, .logout:
                return BaseTableViewCell.height()
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
}

@available(iOS 10.0, *)
extension LeftViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor(hex: "B71E22", alpha: 0.3)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .matches, .chat, .logout:
                let cell = BaseTableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: BaseTableViewCell.identifier)
                cell.setData(menus[indexPath.row])
                return cell
            }
        }
        return UITableViewCell()
    }
}
