//
//  UIActivityIndicatorView.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/14/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

class CustomLoadingHUB:UIActivityIndicatorView {
    
    static private var indicatorView:UIActivityIndicatorView!
    
    static func showHUB(in view:UIView) {
        DispatchQueue.main.async {
            view.isUserInteractionEnabled = false
            indicatorView = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
            indicatorView.color = UIColor.darkGray
            indicatorView.center = view.center
            indicatorView.startAnimating()
            view.addSubview(indicatorView)
        }
    }
    
    static func hideHUB(in view:UIView) {
        DispatchQueue.main.async {
            view.isUserInteractionEnabled = true
            indicatorView.removeFromSuperview()
        }
    }
    
}
