//
//  MatchDetailViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 4/26/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class MatchDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var invitedUsersTableView: UITableView!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var requestTime: UILabel!
    
    var invite:Invite! = Invite()
    var requestedArray:[[String:String]]? = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    func configureView() {
        self.requestDate.text = self.invite.inviteDate
        self.requestTime.text = self.invite.inviteTime
        self.requestedArray = self.invite.invitedPeople
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.requestedArray == nil {
            return 0
        } else {
            return self.requestedArray!.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userInfo:[String:String] = (self.requestedArray?[indexPath.row])!
        let cell:FacebookTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "FacebookTableViewCell") as! FacebookTableViewCell!
        cell.setFacebookImage(facebookID: (userInfo["facebookID"]?.description)!)
        cell.usernameLabel.text = userInfo["fullname"]
        cell.addButton.isHidden = true
        return cell
    }
    
    func showAlert(with title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

}
