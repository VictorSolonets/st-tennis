//
//  CreateChannelCell.swift
//  ST Tennis
//
//  Created by VictorSolo on 4/24/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import UIKit

class CreateChannelCell: UITableViewCell {
    
    @IBOutlet weak var channelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
