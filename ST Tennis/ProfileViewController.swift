//
//  ProfileViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

enum profileSections: Int {
    case profilePhoto = 0
    case profileName
    case profileDescription
}

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var profileTableView: UITableView!
    public var profileImageView: UIImageView!
    public var profileNameLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let dismissKeyboard:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(ProfileViewController.dismissKeyboardAction))
        self.view.addGestureRecognizer(dismissKeyboard)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Profile"
        self.setNavigationBarItem()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    func dismissKeyboardAction() {
        self.view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        User.sharedInstance.description = textField.text
        UserDefaults.standard.set(textField.text, forKey: "ProfileDescription")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let section = profileSections(rawValue: indexPath.row) {
            switch section {
            case .profilePhoto:
                return 200
            case .profileName:
                return 50
            case .profileDescription :
                return 80
            }
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:ProfileTableViewCell!
        if let section = profileSections(rawValue: indexPath.row) {
            switch section {
            case .profilePhoto:
                cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePhotoCell") as! ProfileTableViewCell!
                if User.sharedInstance.avatarURL != nil {
                    cell.profilePhotoImageView.af_setImage(withURL: URL.init(string: User.sharedInstance.avatarURL!)!)
                } else {
                    cell.profilePhotoImageView.image = UIImage.init(named: "mainRocket")
                }
                cell.profilePhotoImageView.layer.cornerRadius = cell.profilePhotoImageView.frame.size.width / 2
                cell.profilePhotoImageView.clipsToBounds = true
                return cell
            case .profileName:
                cell = tableView.dequeueReusableCell(withIdentifier: "ProfileNameCell") as! ProfileTableViewCell!
                cell.nameLabel.text = User.sharedInstance.userName
                return cell
            case .profileDescription :
                cell = tableView.dequeueReusableCell(withIdentifier: "ProfileDescriptionCell") as! ProfileTableViewCell!
                cell.descriptionField.text = User.sharedInstance.description
                return cell
            }
        }
        return UITableViewCell()
    }
}

