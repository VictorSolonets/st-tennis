//
//  InvitePeopleViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation
import UIKit
import FBSDKLoginKit
import Firebase

class CreateInviteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var InviteButton: UIBarButtonItem!
    @IBOutlet weak var facebookTableView: UITableView!
    private lazy var invitesRef: FIRDatabaseReference = FIRDatabase.database().reference().child("invites")
    
    var facebookArray:[FBUser]? = [FBUser]()
    var invite:Invite!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.facebookRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Invite people"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    @IBAction func doneCreateInviteAction(_ sender: Any) {
        if self.requestedArray().count > 1 {
                let newInviteRef = invitesRef.childByAutoId()
                let inviteItem = [
                    "date"          : self.invite.inviteDate,
                    "time"          : self.invite.inviteTime,
                    "invitedPeople" : self.requestedArray()
                ] as [String : Any]
            newInviteRef.setValue(inviteItem)
            if let navigationController = self.navigationController {
                navigationController.popToRootViewController(animated: true)
            }
            
        } else {
            self.showAlert(with: "Warning", message: "Can't create match without any partner")
        }
    }
    
    func requestedArray() -> [[String:String]] {
        var selectedArray = [[String:String]]()
        for fbUser in self.facebookArray! {
            if fbUser.choosed {
                var user = [String:String]()
                user["fullname"] = fbUser.userName
                user["facebookID"] = fbUser.userID
                selectedArray.append(user)
            }
        }
        var mySelf = [String:String]()
        mySelf["fullname"] = User.sharedInstance.userName
        mySelf["facebookID"] = User.sharedInstance.userFBID
        selectedArray.append(mySelf)
        
        return selectedArray
    }
    
    func selectedArray() -> [FBUser] {
        var selectedArray = [FBUser]()
        for fbUser in self.facebookArray! {
            if fbUser.choosed {
                selectedArray.append(fbUser)
            }
        }
        return selectedArray
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.facebookArray == nil {
            return 0
        } else {
            return self.facebookArray!.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userInfo = self.facebookArray?[indexPath.row]
        let cell:FacebookTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "FacebookTableViewCell") as! FacebookTableViewCell!
        cell.setFacebookImage(facebookID: (userInfo?.userID)!)
        cell.usernameLabel.text = userInfo?.userName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.facebookArray![indexPath.row].choosed = true
        self.configureButtonFor(tableView, at: indexPath, isSelected: true)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.facebookArray![indexPath.row].choosed = false
        self.configureButtonFor(tableView, at: indexPath, isSelected: false)
    }
    
    func configureButtonFor(_ tableView:UITableView,at indexPath:IndexPath, isSelected:Bool) {
        let cell:FacebookTableViewCell! = tableView.cellForRow(at: indexPath) as! FacebookTableViewCell!
        cell.addButton.setTitle(isSelected ? "✔︎" : "+", for: .normal)
        cell.addButton.setTitleColor(UIColor.black , for: .normal)
        self.InviteButton.title = self.selectedArray().count > 0 ? "Invite(\(self.selectedArray().count))" : "Invite"
    }
    
    func facebookRequest () {
        if (User.sharedInstance.userFBID != nil) {
            CustomLoadingHUB.showHUB(in:self.view)
            FBSDKGraphRequest(graphPath:"me/friends", parameters: nil).start(completionHandler: {(connection, result, error) -> Void in
                if let resultDict = result as? [String:Any] {
                    if let friends = resultDict["data"] as? [[String:Any]] {
                        for oneFriend in friends {
                            let fbUser = FBUser()
                            if let userID = oneFriend["id"] as? String {
                                fbUser.userID = userID
                            }
                            if let userName = oneFriend["name"] as? String {
                                fbUser.userName = userName
                            }
                            fbUser.choosed = false
                            self.facebookArray?.append(fbUser)
                        }
                        self.facebookTableView.reloadData()
                        CustomLoadingHUB.hideHUB(in:self.view)
                    }
                }
            })
        } else {
            self.showAlert(with: "Warning", message: "You should login from Facebook to see you friends")
        }
    }

    func showAlert(with title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
