//
//  User.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/12/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation
import Firebase

class User {
    
    static let sharedInstance : User = {
        let instance = User()
        return instance
    }()
    
    var userName:String?
    var userFBID:String?
    var avatarURL:String?
    var description:String?
    var firUser:FIRUser?
    
    private init () {
        userName = ""
        userFBID = nil
        avatarURL = nil
        description = ""
        firUser = nil
    }
}
