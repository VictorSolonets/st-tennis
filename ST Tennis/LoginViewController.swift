//
//  LoginViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit
import Foundation
import FBSDKLoginKit
import FirebaseAuth

@available(iOS 10.0, *)
class LoginViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var rocketImageView: UIImageView!
    @IBOutlet weak var topAnonimusConstraints: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Login"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if (UserDefaults.standard.string(forKey: "facebookID") != nil && FBSDKAccessToken.current() != nil) {
            User.sharedInstance.userName = UserDefaults.standard.string(forKey: "fullName")
            User.sharedInstance.userFBID = UserDefaults.standard.string(forKey: "facebookID")
            User.sharedInstance.avatarURL = UserDefaults.standard.string(forKey: "avatarUrl")
            self.navigationToMainViewController()
        }
        
        self.rocketImageView.image = UIImage.init(named: "mainRocket")
        self.rocketImageView.backgroundColor = UIColor.clear
        self.rocketImageView.transform = CGAffineTransform.init(scaleX: 3.0, y: 3.0)
        UIView.animate(withDuration: 1.0) {
            self.rocketImageView.transform = CGAffineTransform.init(scaleX: 0.2, y: 0.2)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func loginAnonAction(_ sender: Any) {
        if (self.nameField?.text == "") {
            self.topAnonimusConstraints.constant = self.topAnonimusConstraints.constant == 10.0 ? 64.0 : 10.0
            UIView.animate(withDuration: 0.3, animations: { 
                self.view.layoutIfNeeded()
                self.nameField.alpha = self.topAnonimusConstraints.constant == 10.0 ? 0.0 : 1.0
            })
        } else {
            FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in
                if let err = error {
                    print(err.localizedDescription)
                    return
                } else {
                    User.sharedInstance.firUser = user
                    User.sharedInstance.userName = self.nameField.text!
                    UserDefaults.standard.set(User.sharedInstance.userName, forKey: "fullName")
                    self.navigationToMainViewController()
                }
            })
        }
    }
    
    @IBAction func loginAction() {
        let login = FBSDKLoginManager()
        CustomLoadingHUB.showHUB(in: self.view)
        
        login.loginBehavior = FBSDKLoginBehavior.systemAccount
        login.logIn(withReadPermissions: ["public_profile", "email", "user_friends"], from: self, handler: {(result, error) in
            if error != nil {
                CustomLoadingHUB.hideHUB(in:self.view)
                print("Error :  \(String(describing: error?.localizedDescription))")
            }
            else if (result?.isCancelled)! {
                CustomLoadingHUB.hideHUB(in:self.view)
            }
            else {
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "first_name, last_name, picture.type(large), email, name, id, gender"]).start(completionHandler: {(connection, result, error) -> Void in
                    if error != nil{
                        print("Error : \(String(describing: error?.localizedDescription))")
                    } else {
                        let responseDict = result as? [String:Any]
                        
                        if let fullName = responseDict?["name"] {
                            User.sharedInstance.userName = fullName as? String
                            UserDefaults.standard.set(fullName, forKey: "fullName")
                        }
                        
                        if let FBid = responseDict?["id"] {
                            UserDefaults.standard.set(FBid, forKey: "facebookID")
                            User.sharedInstance.userFBID = FBid as? String
                        }
                        
                        if let picture = responseDict?["picture"] as? [String:Any] {
                            if let pictureData = picture["data"] as? [String:Any] {
                                if let avatarUrl = pictureData["url"] {
                                    User.sharedInstance.avatarURL = avatarUrl as? String
                                    UserDefaults.standard.set(avatarUrl, forKey: "avatarUrl")
                                }
                            }
                        }
                        let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                        FIRAuth.auth()?.signIn(with: credential, completion: { (currentUser, error) in
                            if error != nil {
//                                print("Error : \(String(describing: error?.localizedDescription))")
                            } else {
                                User.sharedInstance.firUser = currentUser
                                self.navigationToMainViewController()
                                CustomLoadingHUB.hideHUB(in:self.view)
                            }
                        })
                    }
                })
            }
            
        })
    }
    
    @available(iOS 10.0, *)
    func navigationToMainViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        let leftViewController = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = UIColor(hex: "B71E22")
        
        leftViewController.mainViewController = nvc
        
        let slideMenuController = MenuViewController(mainViewController:nvc, leftMenuViewController: leftViewController)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
}
