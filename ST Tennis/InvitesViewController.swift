//
//  InvitesViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit
import Firebase

struct Invite {
    var inviteID : String!
    var inviteDate : String!
    var inviteTime : String!
    var invitedPeople : [[String:String]]!
}

class InvitesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var invitesTableView: UITableView!
    private var invites: [Invite] = []
    private lazy var invitesRef: FIRDatabaseReference = FIRDatabase.database().reference().child("invites")
    private var invitesRefHandle: FIRDatabaseHandle?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observeInvites()
    }
    
    deinit {
        if let refHandle = invitesRefHandle {
            invitesRef.removeObserver(withHandle: refHandle)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Match Requests"
        self.setNavigationBarItem()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func observeInvites() {
        invitesRefHandle = invitesRef.observe(.childAdded, with: { (snapshot) -> Void in
            let invitesData = snapshot.value as! Dictionary<String, AnyObject>
            let id = snapshot.key
            var invite = Invite()
            invite.inviteID = id
            if let date = invitesData["date"] as? String {
                invite.inviteDate = date
            }
            if let inviteTime = invitesData["time"] as? String {
                invite.inviteTime = inviteTime
            }
            if let invitedPeople = invitesData["invitedPeople"] as? [[String:String]] {
                invite.invitedPeople = invitedPeople
            }
            self.invites.append(invite)
            self.invitesTableView.reloadData()
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invites.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let invite = invites[(indexPath as NSIndexPath).row]
        self.performSegue(withIdentifier: "ShowMatchDetail", sender: invite)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MatchRequestCell = tableView.dequeueReusableCell(withIdentifier: "MatchRequestCell", for: indexPath) as! MatchRequestCell
        cell.requestedPeople.text = "\(invites[(indexPath as NSIndexPath).row].invitedPeople.count) users in match"
        cell.requestDate.text = invites[(indexPath as NSIndexPath).row].inviteDate
        cell.requestedTime.text = invites[(indexPath as NSIndexPath).row].inviteTime
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let invite = sender as? Invite {
            let matchDetailViewController = segue.destination as! MatchDetailViewController
            matchDetailViewController.invite = invite
        }
    }

}
