//
//  ServerManager.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/17/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import Foundation
import Alamofire

class ServerManager {
    
    private static var defaultSession:URLSession!
    
    private init() { }
    
    static let sharedInstance : ServerManager = {
        let instance = ServerManager()
        configureAPI()
        return instance
    }()
    
    private static func configureAPI () -> Void {
        let configuredSession = URLSessionConfiguration.default
        configuredSession.timeoutIntervalForRequest = 50
        configuredSession.timeoutIntervalForResource = 50
        configuredSession.allowsCellularAccess = false
        self.defaultSession = URLSession.init(configuration: configuredSession)
    }
    
    private func createRequestWithType(type: String, method: String, header:NSDictionary, parameters:[String:AnyObject]) -> URLRequest {
        var path:String = "\(MAIN_SERVER_URL)\(method)"
        path = path.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        let url = URL.init(string: path)
        var request:URLRequest = URLRequest.init(url: url!)
        request.httpMethod = type
        
        if type == "POST" {
            var str = String()
            for key in parameters.keys {
                str += "\(key)\(String(describing: parameters[key]))"
            }
            let requestData = str.data(using: .utf8)
            request.httpBody = requestData
        } else {
            var str = "?"
            for key in parameters.keys {
                str += "\(key)=\(String(describing: parameters[key]))&"
            }
            path = path.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            request.url = URL.init(string: path)
        }
        request.url = URL.init(string: path)
        return request
    }
    
    func Invite(with date:String, time:String, participants:NSMutableSet, inviteID:String, closure: @escaping (_ responce:URLResponse, _ error:Error) -> Void) {
        let request:URLRequest!
        
        let parameters:[String:AnyObject] = ["date" : date as AnyObject,
                                             "time" : date as AnyObject,
                                             "inviteID" : inviteID as AnyObject,
                                             "participants" : participants as AnyObject]
        
        request = self.createRequestWithType(type: "POST", method: "addInvite", header: [:],
                                             parameters: parameters)
        
        ServerManager.defaultSession.dataTask(with: request) { (data, response, error) in
            print("Error \(error)")
            print("Response \(response)")
            self.callBlockMainThread(closure: closure, responce: response!, error: error ?? NSError())
        }.resume()
    }
    
    private func callBlockMainThread(closure: @escaping (_ responce:URLResponse, _ error:Error) -> Void, responce:URLResponse, error:Error) {
        DispatchQueue.main.async {
            closure(responce, error)
        }
    }
    
}
