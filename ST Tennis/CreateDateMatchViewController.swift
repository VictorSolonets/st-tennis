//
//  CreateDateMatchViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit
import Foundation

class CreateDateMatchViewController: UIViewController {

    @IBOutlet weak var toolBarView: UIView!
    @IBOutlet weak var datePickerView: UIDatePicker!
    @IBOutlet weak var dateInviteButton: UIButton!
    @IBOutlet weak var timeInviteButton: UIButton!
    @IBOutlet weak var bottomDataBickerConstraints: NSLayoutConstraint!
    
    var invete:Invite!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initDataPicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Create Date"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
        self.showDatePicker(show: false)
    }

    func showDatePicker(show:Bool) {
        self.bottomDataBickerConstraints.constant = show ? self.datePickerView.frame.size.height :  -40
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func chooseDateAction(_ sender: Any) {
        self.showDatePicker(show: true)
        self.datePickerView.datePickerMode = .date
    }
    
    @IBAction func chooseTimeAction(_ sender: Any) {
        self.showDatePicker(show: true)
        self.datePickerView.datePickerMode = .time
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.showDatePicker(show: false)
    }
    
    @IBAction func doneAction(_ sender: Any) {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = self.datePickerView.datePickerMode == .date ? "dd MMM YY" : "HH:mm"
        if self.datePickerView.datePickerMode == .date {
            self.dateInviteButton.setTitle(dateFormatter.string(from: self.datePickerView.date), for: .normal)
        } else {
            self.timeInviteButton.setTitle(dateFormatter.string(from: self.datePickerView.date), for: .normal)
        }
        self.showDatePicker(show: false)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? CreateInviteViewController {
            self.invete = Invite()
            self.invete.inviteDate = self.dateInviteButton.titleLabel?.text
            self.invete.inviteTime = self.timeInviteButton.titleLabel?.text
            viewController.invite = self.invete
        }
    }
    
    func initDataPicker() {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateFormat = "dd MMM YY"
        self.dateInviteButton.setTitle(dateFormatter.string(from: Date.init()), for: .normal)
        dateFormatter.dateFormat = "HH:mm"
        self.timeInviteButton.setTitle(dateFormatter.string(from: Date.init()), for: .normal)
        self.datePickerView.backgroundColor = UIColor(hex: "E67168", alpha: 0.3)
    }
}
