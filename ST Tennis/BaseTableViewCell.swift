//
//  BaseTableViewCell.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

open class BaseTableViewCell : UITableViewCell {
    
    class var identifier: String {
        return String.init(describing: self)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    open override func awakeFromNib() {
    }
    
    open func setup() {
    }
    
    open class func height() -> CGFloat {
        return 60.0
    }
    
    open func setData(_ data: Any?) {
        self.backgroundColor = UIColor.clear
        self.textLabel?.font = UIFont.italicSystemFont(ofSize: 18)
        self.textLabel?.textColor = UIColor.white
        if let dict = data as? [String:String] {
            self.textLabel?.text = dict.keys.first
            self.imageView?.image = UIImage(named: dict.values.first!)
        }
        self.selectionStyle = .none
    }
    
    override open func setHighlighted(_ highlighted: Bool, animated: Bool) {
        if highlighted {
            self.alpha = 0.4
        } else {
            self.alpha = 1.0
        }
    }
    
}
