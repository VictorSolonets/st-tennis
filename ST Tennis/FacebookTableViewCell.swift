//
//  FacebookTableViewCell.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/14/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit

class FacebookTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setFacebookImage(facebookID:String) {
        let userID:String = facebookID
        let avatarString:String = "https://graph.facebook.com/\(userID)/picture?type=large" as String!
        let avatarURL:URL = URL.init(string: avatarString)!
        self.avatarImageView.af_setImage(withURL: avatarURL)
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.width / 2
        self.avatarImageView.clipsToBounds = true
    }

}
