//
//  ChatViewController.swift
//  ST Tennis
//
//  Created by VictorSolo on 4/24/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import Photos
import Firebase
import JSQMessagesViewController

@available(iOS 10.0, *)
final class ChatViewController: JSQMessagesViewController {
    
    var messages = [JSQMessage]()
    var refreshMessages = [JSQMessage]()
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    private var updatedMessageRefHandle: FIRDatabaseHandle?
    
    private lazy var usersTypingQuery: FIRDatabaseQuery =
        self.channelRef!.child("typingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    
    private lazy var userIsTypingRef: FIRDatabaseReference =
        self.channelRef!.child("typingIndicator").child(self.senderId)
    
    lazy var storageRef: FIRStorageReference = FIRStorage.storage().reference(forURL: "gs://st-tennis.appspot.com")
    private let imageURLNotSetKey = "NOTSET"
    var queryLimitTo:UInt! = 0
    private var photoMessageMap = [String: JSQPhotoMediaItem]()
    
    private var localTyping = false
    
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    
    private lazy var messageRef: FIRDatabaseReference = self.channelRef!.child("messages")
    private var newMessageRefHandle: FIRDatabaseHandle?
    
    var channelRef: FIRDatabaseReference?
    var channel: Channel? {
        didSet {
            title = channel?.name
            self.senderId = FIRAuth.auth()?.currentUser?.uid
        }
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.observeMessages(isAddBegin: false)
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize(width: 0.0, height: 0.0)
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: 0.0, height: 0.0)
        let refreshControll = UIRefreshControl()
        refreshControll.addTarget(self, action: #selector(ChatViewController.refreshMessageHistory), for: .valueChanged)
        self.collectionView.refreshControl = refreshControll;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        observeTyping()
    }
    
    func refreshMessageHistory() {
        observeMessages(isAddBegin: true)
        refreshMessages.reverse()
        for i in 0..<refreshMessages.count {
            messages.insert(refreshMessages[i], at: 0)
        }
        refreshMessages = [JSQMessage]()
    }
    
    private func observeTyping() {
        let typingIndicatorRef = channelRef!.child("typingIndicator")
        userIsTypingRef = typingIndicatorRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()
        usersTypingQuery.observe(.value) { (data: FIRDataSnapshot) in
            if data.childrenCount == 1 && self.isTyping {
                return
            }
            self.showTypingIndicator = data.childrenCount > 0
            self.scrollToBottom(animated: true)
        }
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        let itemRef = messageRef.childByAutoId()
        let messageItem = [
            "senderId": senderId!,
            "senderName": senderDisplayName!,
            "text": text!,
            ]
        itemRef.setValue(messageItem)
        isTyping = false
        finishSendingMessage()
    }
    
    private func observeMessages(isAddBegin:Bool) {
        messageRef = channelRef!.child("messages")
        queryLimitTo = queryLimitTo + 25
        let messageQuery = messageRef.queryLimited(toLast:queryLimitTo)
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData = snapshot.value as! Dictionary<String, String>
            self.collectionView.refreshControl?.endRefreshing()
            if let id = messageData["senderId"] as String!, let name = messageData["senderName"] as String!, let text = messageData["text"] as String!, text.characters.count > 0 {
                self.addMessage(withId: id, name: name, text: text, isAddStart: isAddBegin)
                self.finishReceivingMessage()
            } else if let id = messageData["senderId"] as String!,
                let photoURL = messageData["photoURL"] as String! {
                if let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: id == self.senderId) {
                    self.addPhotoMessage(withId: id, name: "", key: snapshot.key, mediaItem: mediaItem, isAddStart:isAddBegin)
                    if photoURL.hasPrefix("gs://") {
                        self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: nil)
                    }
                    self.finishReceivingMessage()
                }
            } else {
                print("Error! Could not decode message data")
            }
        })
        
        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
            let key = snapshot.key
            let messageData = snapshot.value as! Dictionary<String, String>
            if let photoURL = messageData["photoURL"] as String! {
                if let mediaItem = self.photoMessageMap[key] {
                    self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem, clearsPhotoMessageMapOnSuccessForKey: key)
                }
            }
        })
    }
    
    private func addMessage(withId id: String, name: String, text: String, isAddStart: Bool) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            if self.equalMessage(message: message) {
                if isAddStart {
                    refreshMessages.append(message)
                } else {
                    messages.append(message)
                }
            }
        }
    }
    
    private func addPhotoMessage(withId id: String, name: String, key: String, mediaItem: JSQPhotoMediaItem, isAddStart: Bool) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            if self.equalMessage(message: message) {
                if isAddStart {
                    refreshMessages.append(message)
                } else {
                    messages.append(message)
                }
                if (mediaItem.image == nil) {
                    photoMessageMap[key] = mediaItem
                }
            }
        }
    }
    
    private func equalMessage(message: JSQMessage) -> Bool {
        var result = true
        for oneMessage in self.messages {
            if message.isMediaMessage {
                if message.media.isEqual(oneMessage.media) {
                    result = false
                }
            } else if (!oneMessage.isMediaMessage &&
                message.text == oneMessage.text &&
                message.senderId == oneMessage.senderId) {
                result = false
            }
        }
        return result
    }
    
    func sendPhotoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        
        let messageItem = [
            "photoURL": imageURLNotSetKey,
            "senderId": senderId!,
            ]
        
        itemRef.setValue(messageItem)
        
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        
        finishSendingMessage()
        return itemRef.key
    }
    
    private func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        let storageRef = FIRStorage.storage().reference(forURL: photoURL)
        storageRef.data(withMaxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            storageRef.metadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                if (metadata?.contentType == "image/gif") {
                    mediaItem.image = UIImage.init(named: "mainRocket")
                } else {
                    mediaItem.image = UIImage.init(data: data!)
                }
                self.collectionView.reloadData()
                
                guard key != nil else {
                    return
                }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        }
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["photoURL": url])
    }
    
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        self.isTyping = textView.text != ""
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func didPressAccessoryButton(_ sender: UIButton) {
        let picker = UIImagePickerController()
        picker.delegate = self
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            picker.sourceType = UIImagePickerControllerSourceType.camera
        } else {
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        
        present(picker, animated: true, completion:nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        cell.avatarImageView.layer.cornerRadius = 15.0
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
            cell.avatarImageView.backgroundColor = UIColor.blue
        } else {
            cell.textView?.textColor = UIColor.black
            cell.avatarImageView.backgroundColor = UIColor.gray
        }
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
}
