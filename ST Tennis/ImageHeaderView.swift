//
//  ImageHeaderView.swift
//  ST Tennis
//
//  Created by VictorSolo on 12/9/16.
//  Copyright © 2016 victorsolonetsedition. All rights reserved.
//

import UIKit
import AlamofireImage

class ImageHeaderView : UIView {
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor(hex: "B71E22", alpha: 0.3)
        self.name.textColor = UIColor.white
        self.avatarImage.layer.cornerRadius = self.avatarImage.bounds.size.height / 2
        self.avatarImage.clipsToBounds = true
        self.avatarImage.layer.borderWidth = 1
        self.avatarImage.layer.borderColor = UIColor.white.cgColor
    }
    
    func setProfileImage(avatarURL:NSURL) {
        self.avatarImage.af_setImage(withURL: avatarURL as URL)
    }
}
