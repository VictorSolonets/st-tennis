//
//  MatchRequestCell.swift
//  ST Tennis
//
//  Created by VictorSolo on 4/26/17.
//  Copyright © 2017 victorsolonetsedition. All rights reserved.
//

import UIKit

class MatchRequestCell: UITableViewCell {
    
    @IBOutlet weak var requestedPeople: UILabel!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var requestedTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
